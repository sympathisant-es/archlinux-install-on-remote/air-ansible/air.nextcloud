Ansible Archlinux Nextcloud
==========================

A role to install nextcloud and set basic config

It also install apache, ssl cert, php-fpm, redis and mariadb

Tested and Used on ArchLinux but it may work on any Linux

Dependencies
------------

[Requirements](molecule/default/requirements.yml)


Variables
---------

[Role Variables](defaults/main.yml)


Example Playbook
----------------

[Test Playbook](molecule/default/converge.yml)


License
-------

[License](LICENSE)

How To Test
-----------

You must have Ansible, Molecule and Podman installed on your Archlinux Host.

Run:

``` shell
mkdir -p Air/roles
cd Air/roles

# clone repo
git clone https://git.laquadrature.net/sympathisant-es/archlinux-install-on-remote/air-ansible/air.nextcloud.git

# start nextcloud in molecule
cd air.nextcloud
molecule  test --destroy=never

```

Open Firefox and Goto https://localhost:18443/

Accept Self Signed Certificat

And Login as alice/superadmin (maybe you will have to retry login once, I don't know why)

How to Install Nextcloud
------------------------

You must have run **molecule test** before !

Then [Read The Doc](README_HowToUseIt.md)

Todo
----
- Be Kind
